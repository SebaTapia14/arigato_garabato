import mysql.connector
from flask import Flask, render_template, request

app = Flask(__name__)

# Configuración de la conexión a la base de datos
config = {
    'user': 'root',
    'password': 'root',
    'host': 'localhost',
    'database': 'tiendaonline',
    'port': '3307',
    'raise_on_warnings': True
}

# Conexión a la base de datos
conn = mysql.connector.connect(**config)


def crear_tabla_productos():
    # Crea un cursor para ejecutar consultas SQL
    cursor = conn.cursor()

    # Verifica si la tabla 'productos' existe en la base de datos
    cursor.execute("SHOW TABLES LIKE 'productos'")
    table_exists = cursor.fetchone()

    if not table_exists:
        # Crea la tabla de productos si no existe
        cursor.execute('''
            CREATE TABLE productos (
                id INT AUTO_INCREMENT PRIMARY KEY,
                nombre VARCHAR(100) NOT NULL,
                precio DECIMAL(10, 2) NOT NULL
            )
        ''')

        # Confirma los cambios en la base de datos
        conn.commit()

    # Cierra el cursor
    cursor.close()


def agregar_al_carrito(nombre, precio):
    # Crea un cursor para ejecutar consultas SQL
    cursor = conn.cursor()

    # Inserta el producto en la base de datos
    cursor.execute('INSERT INTO productos (nombre, precio) VALUES (%s, %s)', (nombre, precio))

    # Confirma los cambios en la base de datos
    conn.commit()

    # Cierra el cursor
    cursor.close()

    print('Producto agregado al carrito')


# Ruta para la página principal
@app.route("/")
def index():
    return render_template("carrito.html")


# Ruta para mostrar los productos en el carrito
@app.route("/productos")
def mostrar_productos():
    # Crea un cursor para ejecutar consultas SQL
    cursor = conn.cursor()

    # Realiza una consulta para obtener los productos en el carrito
    cursor.execute("SELECT * FROM productos")
    productos = cursor.fetchall()

    # Cierra el cursor
    cursor.close()

    # Renderiza una plantilla para mostrar los productos
    return render_template("productos.html", productos=productos)


# Ruta para agregar un producto al carrito
@app.route("/agregar", methods=["POST"])
def agregar():
    nombre = request.form.get("nombre")
    precio = request.form.get("precio")
    agregar_al_carrito(nombre, precio)
    return "Producto agregado al carrito"


if __name__ == '__main__':
    # Ejecuta la función para crear la tabla de productos
    crear_tabla_productos()
    
    # Ejecuta el servidor Flask
    app.run()
