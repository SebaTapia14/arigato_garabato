import mysql.connector
from flask import Flask, render_template, request
from werkzeug.security import generate_password_hash, check_password_hash

# Conectar a la base de datos MySQL
conexion = mysql.connector.connect(
    host='localhost',
    user='root',
    password='root',
    database='tiendaonline',
    port='3307'
)

# Crear la tabla "usuarios" si no existe
def crear_tabla_usuarios():
    cursor = conexion.cursor()
    cursor.execute("""
        CREATE TABLE IF NOT EXISTS usuarios (
            id INT AUTO_INCREMENT PRIMARY KEY,
            email VARCHAR(255) NOT NULL,
            password VARCHAR(255) NOT NULL
        )
    """)
    conexion.commit()
    print("La tabla 'usuarios' ha sido creada correctamente.")

# Llamar a la función para crear la tabla
crear_tabla_usuarios()

# Crear una instancia de Flask
app = Flask(__name__)

# Ruta para la página de inicio de sesión
@app.route("/")
def index():
    return render_template("index.html")

# Ruta para la página de carrito
@app.route("/carrito")
def carrito():
    return render_template("carrito.html")

# Ruta para la página de contacto
@app.route("/contacto")
def contacto():
    return render_template("contacto.html")

# Ruta para la página de recuperar contraseña
@app.route("/forgot-password")
def forgot_password():
    return render_template("forgot-password.html")

# Ruta para la página de login
@app.route("/login")
def login():
    return render_template("login.html")

# Ruta para la página de nosotros
@app.route("/nosotros")
def nosotros():
    return render_template("nosotros.html")

# Ruta para la página de registro
@app.route("/signup")
def signup():
    return render_template("signup.html")

if __name__ == '__main__':
    app.run()

# Ruta para manejar la solicitud POST del formulario de inicio de sesión
@app.route('/login', methods=['POST'])
def login():
    email = request.form.get('email')
    password = request.form.get('password')

    # Verificar si el usuario existe en la base de datos
    cursor = conexion.cursor()
    query = "SELECT * FROM usuarios WHERE email = %s"
    cursor.execute(query, (email,))
    user = cursor.fetchone()

    if user:
        # Verificar la contraseña
        stored_password = user[2]
        if check_password_hash(stored_password, password):
            return "Inicio de sesión exitoso"
        else:
            return "Contraseña incorrecta"
    else:
        return "Usuario no encontrado"

# Ejecutar la aplicación Flask
if __name__ == '__main__':
    app.run()
