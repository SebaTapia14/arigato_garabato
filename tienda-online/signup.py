from flask import Flask, request
import mysql.connector

app = Flask(__name__)

# Establecer la conexión a la base de datos
conexion = mysql.connector.connect(
    host='localhost',
    user='root',
    password='root',
    database='tiendaonline',
    port='3307'
)

# Crear la tabla de usuarios si no existe
crear_tabla_usuarios = """
CREATE TABLE IF NOT EXISTS usuarios (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombre_completo VARCHAR(100) NOT NULL,
    correo_electronico VARCHAR(100) NOT NULL,
    numero_telefono VARCHAR(20) NOT NULL,
    fecha_nacimiento DATE NOT NULL,
    genero VARCHAR(20) NOT NULL,
    direccion VARCHAR(200) NOT NULL,
    codigo_postal VARCHAR(10) NOT NULL,
    pais VARCHAR(100) NOT NULL,
    region VARCHAR(100) NOT NULL,
    ciudad VARCHAR(100) NOT NULL
)
"""
cursor = conexion.cursor()
cursor.execute(crear_tabla_usuarios)

# Función para insertar un nuevo usuario en la base de datos
def crear_usuario(nombre_completo, correo_electronico, numero_telefono, fecha_nacimiento, genero, direccion, codigo_postal, pais, region, ciudad):
    insertar_usuario = """
    INSERT INTO usuarios (nombre_completo, correo_electronico, numero_telefono, fecha_nacimiento, genero, direccion, codigo_postal, pais, region, ciudad)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """
    datos_usuario = (nombre_completo, correo_electronico, numero_telefono, fecha_nacimiento, genero, direccion, codigo_postal, pais, region, ciudad)
    cursor.execute(insertar_usuario, datos_usuario)
    conexion.commit()

# Ruta para manejar la creación de usuarios
@app.route('/signup', methods=['POST'])
def signup():
    nombre_completo = request.form['nombre_completo']
    correo_electronico = request.form['correo_electronico']
    numero_telefono = request.form['numero_telefono']
    fecha_nacimiento = request.form['fecha_nacimiento']
    genero = request.form['genero']
    direccion = request.form['direccion']
    codigo_postal = request.form['codigo_postal']
    pais = request.form['pais']
    region = request.form['region']
    ciudad = request.form['ciudad']

    crear_usuario(nombre_completo, correo_electronico, numero_telefono, fecha_nacimiento, genero, direccion, codigo_postal, pais, region, ciudad)

    return 'Usuario creado exitosamente'

# Cerrar la conexión a la base de datos
conexion.close()

if __name__ == '__main__':
    app.run()
